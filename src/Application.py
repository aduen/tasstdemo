'''
Created on Sep 21, 2012

@author: aduen
'''
from Controller import SerialCommController, \
    LogController, ContextController, DatabaseController, ApplicationController,\
    FlashSocketController
from MVCFramework.MVCBase import ApplicationContext
from Model import SerialCommunication, DataLog, DatabaseModel, FlashSock
from Signals import ContextSignal, SerialPortSignal,  \
    LogSignal, DatabaseSignal, SocketSignal, SocketDataSignal
from Tkinter import Tk

class MainContext(ApplicationContext):
    '''
    classdocs
    
    Simple MVC implementation as far as I was able to take it
    '''
    
    tk = None

    def __init__(self):
        '''
        Constructor
        '''
        self.tk = Tk()
        ApplicationContext.__init__(self)
        
        #construct models
        ser_comm = SerialCommunication()
        flash_socket = FlashSock()
        data_logger = DataLog()
        database_model = DatabaseModel()
        
        #set up controllers
        appl_controller = ApplicationController(model=self.tk)
        context_controller = ContextController()
        ser_com_controller = SerialCommController(model=ser_comm)
        fsocket_controller = FlashSocketController(flash_socket)
        log_controller = LogController(model=data_logger)
        db_controller = DatabaseController(model=database_model)
        
        #add event mappings
        self.addListener(ContextSignal.APPLICATION_START, context_controller.onEvent)
        
        self.addListener(SerialPortSignal.OPEN, ser_com_controller.onEvent)
        self.addListener(SerialPortSignal.CLOSE, ser_com_controller.onEvent)
        self.addListener(SerialPortSignal.RECEIVE_DATA, ser_com_controller.onEvent)
        self.addListener(SerialPortSignal.TRANSMIT_DATA, ser_com_controller.onEvent)
        
        self.addListener(ContextSignal.APPLICATION_START, fsocket_controller.onEvent)
        self.addListener(ContextSignal.APPLICATION_CLOSE, fsocket_controller.onEvent)
        
        self.addListener(SerialPortSignal.RECEIVE_DATA, fsocket_controller.onEvent)
        
        self.addListener(LogSignal.SAVE_ENTRY, log_controller.onEvent)
        self.addListener(LogSignal.SAVE_LOG, log_controller.onEvent)
        self.addListener(LogSignal.START_LOG, log_controller.onEvent)
        
        self.addListener(DatabaseSignal.OPEN, db_controller.onEvent)
        self.addListener(DatabaseSignal.SELECT_DATA, db_controller.onEvent)
        self.addListener(DatabaseSignal.STORE_DATA, db_controller.onEvent)
        
        self.addListener(SocketSignal.SOCKET_PY_CLOSED, appl_controller.onEvent)
        
        #bring in the loop
        #self.tk.withdraw()
        self.notifyListener(ContextSignal.APPLICATION_START, 'value', 'message')
        self.tk.mainloop()
       
        self.notifyListener(ContextSignal.APPLICATION_CLOSE, 'value', 'message')
        #done
        
        #destruct application
        print "destorying software"
        data_logger.save()
        print "data logger saved..."
        ser_comm.disconnect()
        print "serial interface disconnected"
        flash_socket.stop()
        print "application socket disconnected"
        self.__del__()
        
