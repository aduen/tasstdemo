'''
Created on Sep 21, 2012

@author: aduen
'''
from MVCFramework.EventContext import Signal

class ContextSignal(Signal):
    APPLICATION_START = 'applicationStart'
    APPLICATION_CLOSE = 'applicationClose'
    SHUTDOWN_APPLICATION = 'shutdownApplication'
    
    def __init__(self, source, typed, value, message):
        Signal.__init__(self, source, typed, value, message)
        
class SerialPortSignal(Signal):
    OPEN = 'openPort'
    CLOSE = 'closePort'
    RECEIVE_DATA = 'recvSerData'
    TRANSMIT_DATA = 'transmitSerData'
    CONNECTED = 'serialportConnected'
    
    def __init__(self, source, typed, value, message):
        Signal.__init__(self, source, typed, value, message)

class LogSignal(Signal):
    SAVE_ENTRY = 'saveEntry'
    SAVE_LOG_FILE = 'saveLogFile'
    START_LOG = 'startLog'
    SAVE_LOG = 'saveLog'
    
    def __init__(self, source, typed, value, message):
        Signal.__init__(self, source, typed, value, message)
    
class DatabaseSignal(Signal):
    SELECT_DATA = 'selectData'
    STORE_DATA = 'storeData'
    OPEN = 'openDB'
    
    def __init__(self, source, typed, value, message):
        Signal.__init__(self, source, typed, value, message)
        
        
class CommandLineSignal(Signal):
    PROCES_COMMAND = 'procesCommand'
    COMMAND_COMPLETE = 'commandComplete'
    COMMAND_ERROR = 'commandError'
    
    def __init__(self, source, typed, value, message):
        Signal.__init__(self, source, typed, value, message)

class SocketDataSignal(Signal):
    DATA_INCOMING = 'dataIncoming'
    DATA_ERROR = 'dataError'

    def __init__(self, source, typed, value, message):
        Signal.__init__(self, source, typed, value, message)
        
class SocketSignal(Signal):
    SOCKET_PY_CLOSED = 'socketPyClosed'
    SOCKET_PY_STARTED = 'socketPyStarted'
    SOCKET_PY_ERROR = 'socketPyError'
    
    SOCKET_ZMQ_CLOSED = 'socketZMQClosed'
    SOCKET_ZMQ_STARTED = 'socketZMQStarted'
    SOCKET_ZMQ_ERROR = 'socketZMQError'
    
    def __init__(self, source, typed, value, message):
        Signal.__init__(self, source, typed, value, message)