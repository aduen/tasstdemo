'''
Created on Sep 21, 2012

@author: aduen
'''
from MVCFramework.MVCBase import Model
from Signals import SerialPortSignal, CommandLineSignal, SocketSignal
from threading import Thread
import sqlite3
import csv
import glob
import os
import serial
import shutil
import time
import subprocess
import socket
import sys

class DatabaseModel(Model):
    '''
    Database store model
    '''
    
    __db__ = None
    __is_connected__ = False
    
    def __init__(self):
        Model.__init__(self)
        
    def open_db(self, dbname='test.db'):
        self.__db__ = sqlite3.connect(dbname)
        self.c = self.__db__.cursor()
        
        if(self.__db__):
            self.__is_connected__ = True
            print 'Database:', dbname, 'is connected'
        
    def store_value(self, value):
        if not(self.__is_connected__):return
        
        pass
    
    def select_value(self, params):
        if not(self.__is_connected__):return
        
        for row in self.c.execute("SELECT * FROM test"):
            print row

class DataLog(Model):
    
    __log_file__ = None
    __is_started = False
    
    def __init__(self):
        Model.__init__(self)
        self.writer = None
        self.path = '../logs/'
        self.file_name = None
        self.num_logs = 0
        pass
    
    def start(self, path='../logs/', name='tmp_log'):
        if not os.path.exists(path):
            os.makedirs(path)

        self.path = path
        self.file_name = name
        
        self.writer, self.__log_file__ = self.__open_tmp_filewriter__(name)
        
        for infile in glob.glob(os.path.join(self.path, '*.log')):
                self.num_logs = self.num_logs + 1
        
        self.__is_started = True
    
    def write_entry(self, values):
        if not(self.__is_started):return
        
        time_stamp = time.asctime()
        values.append(time_stamp)
        self.__write_to_file__([values])
    
    def save(self):
        if(self.__is_started and os.path.exists(os.path.abspath(self.__log_file__.name))):
            name = self.file_name + str(self.num_logs)
            os.rename(os.path.abspath(self.__log_file__.name), name + '.log')
            shutil.move('./' + name + '.log', self.path + name + '.log')
    
    def __open_tmp_filewriter__(self, name):
        tmppath = os.path.curdir + '/' + name + '.csv'

        tmpfile = open(tmppath, 'wb', buffering=0)
        writer = csv.writer(tmpfile)
        return writer, tmpfile
    
    def __write_to_file__(self, rows):
        self.writer.writerows(rows)
        pass

class CommandLineModel(Model):
    '''
    Command line toolkit, process command and get repsonse
    
    misschien handig als je die printer vanaf de commandline wilt aansturen
    '''
    
    def __init__(self):
        Model.__init__(self)
        pass
    
    def proces_cmd(self, cmd, *params):
        command = params
        command.insert(0, cmd)
        
        try :
            p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            for line in p.stdout:
                print line,
                self.context.notifyListener(CommandLineSignal.COMMAND_COMPLETE, cmd, line)
            p.kill()
        except :
            self.context.notifyListener(CommandLineSignal.COMMAND_ERROR, cmd, '')
        

class SerialDataAnalyzer(Model):
    '''
    Serial Data Analyzer
    '''
    
    def __init__(self):
        Model.__init__(self)
        
    def parse_data(self, data):
        pass

        

class SerialCommunication(Model):
    '''
    Serial communication model
    '''
    
    BAUDRATE = 9600
    DEVICE = '/dev/tty'
    
    NULL_SPACE = 0x0
    ACTIVATE = 0x01
    DEACTIVATE = 0x02

    LED_ON = 0x03
    LED_OFF = 0x04

    LED_GREEN = 0x2E# 46
    LED_RED = 0x2F# 47
    LED_YELLOW = 0x30# 48

    ENABLE_MONITOR = 0x05
    DISABLE_MONITOR = 0x06
    MONITOR_UPDATE = 0x32
    
    ECHO_MESSAGE = 0x10
  
    __serialThread__ = None
    
    def __init__(self):
        Model.__init__(self)
        
        self.comm_thread = 0
    
    def connect(self, com_port):
        self.DEVICE = com_port
        self.ser = serial.Serial(self.DEVICE, self.BAUDRATE)
        
        if(self.ser.isOpen()):
            print 'starting serial thread'
            self.__serialThread__ = SerialMessageThread(self.ser, context=self.context)
            self.__serialThread__.start()
            
            on_byte = bytearray()
            on_byte.append(self.LED_ON)
            on_byte.append(self.LED_GREEN)
            on_byte.append(self.ACTIVATE)

            self.context.notifyListener(SerialPortSignal.CONNECTED, self.DEVICE, '')
            
    def disconnect(self):
        if(self.__serialThread__ is None):return
        
        off_byte = bytearray()
        off_byte.append(self.LED_OFF)
        off_byte.append(self.LED_GREEN)
        off_byte.append(self.ACTIVATE)
        #self.add_byte_command(off_byte)
        
        #time.sleep(1)
        
        self.__serialThread__.stopLoop()
        self.ser.close()
        
    def add_byte_command(self, cmd):
        self.__serialThread__.push_byte(cmd)


class SerialMessageThread(Thread):
    loop = True
    send_data = []
    time_out = 0.1
    
    def __init__(self, serial, context=None):
        Thread.__init__(self)
        self.daemon = False
        self.serial = serial
        
        self.context = context
        
        self.serial.timeout = self.time_out
    
    def push_message(self, msg):
        byte = bytearray()
        byte.append(msg)
        self.send_data.append(byte)
        
    def push_byte(self, byte_msg):
        self.send_data.append(byte_msg)
    
    def stopLoop(self):
        self.loop = False
        self.join()
        
    def run(self):
        while(self.loop):
            try :
                #sending data
                if(len(self.send_data) > 0):
                    msgsnd = self.send_data.pop(0)
                    self.serial.write(msgsnd)
                
                #receiving data
                if(self.serial.inWaiting() > 4):
                    header = self.serial.read(1).encode('hex')
                    
                    #check package
                    if(int(header, 16) == 0xAA):
                        msgrecv = self.serial.read(3).encode('hex')
                        footer = self.serial.read(1).encode('hex')
                    else : 
                        continue
                    
                    if(int(footer, 16) == 0xDD):
                        #print "id: ", int(msgrecv, 16) >> 16, "value: ", int(msgrecv, 16) & 0x00FFFF
                        msg = ( (int(msgrecv, 16) >> 16), int(msgrecv, 16) & 0x00FFFF)
                        self.context.notifyListener(SerialPortSignal.RECEIVE_DATA, msg, 'id,value')
                
                #time.sleep(0.01)
            except Exception, e:
                if(str(e) == "timed out"):continue
                    
                print 'ERROR: in Serial Thread; ', e
            time.sleep(0.001)

class FlashSock(Model):
    '''
    maintain connection with Flash
    '''
    
    __socket_thread__ = None
    
    def __init__(self):
        Model.__init__(self)
        
        self.socket = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind (('', 5001))
        self.socket.listen (5)
        self.socket.settimeout(1)
        print 'socket started'
        
    def run(self):
        self.__socket_thread__ = PySockThread(self.socket, notifier = self.context)
        self.__socket_thread__.start()
        print 'socket thread started'
    
    def send(self, message):
        self.__socket_thread__.push(message)
        
    def stop(self):
        if(self.__socket_thread__):
            self.__socket_thread__.stopLoop()
            
        if(self.socket):
            self.socket.close()
        
    def __parse_command__(self):
        pass
    
class PySockThread(Thread):      
    loop = True
    send_data = []
    
    def __init__(self, socket, **kwargs):
        Thread.__init__(self)
        
        #find out where the messages are getting stuck
        
        self.socket = socket
        self.context = kwargs["notifier"]
        self.daemon = False
        
        self.delay = 0.01
    
    def push(self, message):
        self.send_data.append(message)
        pass
      
    def stopLoop(self):
        self.loop = False
        self.join()
    
    def run(self):
        channel = None
        details = None
        
        sys.stdout.flush()
        sys.stdout.write( "server_start" )
        sys.stdout.flush()
        for i in range(40):
            if(self.loop == False): break
            try :
                channel, details = self.socket.accept()
                break
            except Exception, e :
                sys.stdout.flush()
                sys.stdout.write( "unable to connect" )
            
        if channel != None :
            #channel.setblocking(0)
            channel.settimeout(0.001)
            print "Application Socket connected on: ", details
        else :
            self.loop = False
            print "Cannot connect to application, is the port correct (5001)"
        
        while(self.loop):
            try :
                while(len(self.send_data) > 0):
                    msg = self.send_data.pop(0)
                    #check the timeouts...
                    channel.send (msg)
                
                cmd = channel.recv (2048)
                
                if not cmd=="":
                    if cmd.strip() == "exit":
                        self.socket.close()
                        channel.close()
                        self.stopLoop()
                        break
    
            except Exception, e:
                if(str(e) == "timed out"):continue
                
                print 'ERROR: on application socket; ', e
        else:
            if not(channel == None): channel.close()
            if self.context: self.context.notifyListener(SocketSignal.SOCKET_PY_CLOSED, self.socket, 'closed')
            print 'leaving Application Socket Thread'
            