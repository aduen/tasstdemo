'''
Created on Oct 16, 2012

@author: aduen
'''

class ChebyshevFilter():
    '''
    classdocs
    '''


    def __init__(self,order = 1):
        '''
        Constructor
        '''
        self.__order__ = order
        self._v = [1,1,1]

        
    def compute(self, data):
        try:
            if(self.__order__ == 1):  #ripple -3dB
                self._v[0] = self._v[1]
                tmp = ((((data * 3269048) >>  2) + ((self._v[0] * 3701023) >> 3))+1048576) >> 21
                self._v[1]= tmp
                return int(self._v[0] + self._v[1])
            if(self.__order__ == 2):  #ripple -1dB
                self._v[0] = self._v[1]
                self._v[1] = self._v[2]
                tmp = ((((data * 662828) >>  4) + ((self._v[0] * -540791) >> 1) + (self._v[1] * 628977))+262144) >> 19
                self._v[2]= tmp
                return int(((self._v[0] + self._v[2]) +2 * self._v[1]))
        except ZeroDivisionError, e:
            print 'filter: ', e.message
        
class BesselFilter():
    '''
    classdocs
    '''


    def __init__(self,order = 1):
        '''
        Constructor
        '''
        self.__order__ = order
        self._v = [1,1,1]
        
    def compute(self, data):
        try:
            if(self.__order__ == 1):  #Alpha Low 0.1
                self._v[0] = self._v[1]
                tmp = ((((data * 2057199) >>  3) + ((self._v[0] * 1068552) >> 1))+524288) >> 20
                self._v[1]= tmp
                return self._v[0] + self._v[1]
            if(self.__order__ == 2):  #Alpha Low 0.1
                self._v[0] = self._v[1]
                self._v[1] = self._v[2]
                tmp = ((((data * 759505) >>  4)+ ((self._v[0] * -1011418) >> 3) + ((self._v[1] * 921678) >> 1))+262144) >> 19
    
                self._v[2]= tmp
                return (self._v[0] + self._v[2])+2 * self._v[1]
        except ZeroDivisionError, e:
            print 'filter:', e.message
        