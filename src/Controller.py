'''
Created on Sep 21, 2012

@author: aduen
'''
from MVCFramework.MVCBase import Controller
from Model import SerialCommunication
from Signals import SerialPortSignal, LogSignal, ContextSignal, DatabaseSignal,\
    SocketSignal
from serial.tools.list_ports import comports

class ApplicationController(Controller):
    '''
    handle Tk application
    '''
    
    __model__ = None
    
    def __init__(self, model = None):
        Controller.__init__(self)
        self.__model__ = model
        
    def onEvent(self, evt):
        if(evt.typed() == SocketSignal.SOCKET_PY_CLOSED):
            self.__model__.quit()

class ContextController(Controller):
    '''
    handle MVC context signals
    '''
    
    __model__ = None
    
    def __init__(self, model = None):
        Controller.__init__(self)
        self.__model__ = model
        
    def onEvent(self, evt):
        if(evt.typed() == ContextSignal.APPLICATION_START):
            print "----- debug -----"
            print "application started"
            print 'try connecting...', comports()[-1][0]
            self.context.notifyListener(SerialPortSignal.OPEN, comports()[-1][0], 'message')
            self.context.notifyListener(DatabaseSignal.OPEN, 'test.db', 'message')
            self.context.notifyListener(DatabaseSignal.SELECT_DATA, 'value', 'message')

class DatabaseController(Controller):
    
    __model__ = None
    
    def __init__(self, model = None):
        Controller.__init__(self)
        self.__model__ = model
    
    def onEvent(self, evt):
        if(evt.typed() == DatabaseSignal.OPEN):
            self.__model__.open_db(evt.value())
            
        if(evt.typed() == DatabaseSignal.SELECT_DATA):
            self.__model__.select_value('dummy')
            
            
class SerialCommController(Controller):
    
    __model__ = None
    
    def __init__(self, model=None):
        Controller.__init__(self)
        self.__model__ = model
        
    def onEvent(self, evt):
        if(evt.typed() == SerialPortSignal.OPEN):
            self.__model__.connect(evt.value())
        
        if(evt.typed() == SerialPortSignal.RECEIVE_DATA):
            #print evt.value()
            pass
        
        if(evt.typed() == SerialPortSignal.TRANSMIT_DATA):
            self.__model__.add_byte_command(evt.value())
            
            
class LogController(Controller):
    
    __model__ = None
    
    def __init__(self, model=None):
        Controller.__init__(self)
        self.__model__ = model
        
    def onEvent(self, evt):
        if(evt.typed() == LogSignal.SAVE_ENTRY):
            self.__model__.write_entry(evt.value())
        if(evt.typed() == LogSignal.SAVE_LOG):
            self.__model__.save()
        if(evt.typed() == LogSignal.START_LOG):
            path, name = evt.value()
            self.__model__.start(path, name)
            pass

class FlashSocketController(Controller):
    
    __model__ = None
    
    def __init__(self, model):
        Controller.__init__(self)
        self.__model__ = model
        
    def onEvent(self, evt):
        if(evt.typed() == ContextSignal.APPLICATION_START):
            self.__model__.run()
            return
        if(evt.typed() == ContextSignal.APPLICATION_CLOSE):
            self.__model__.stop()
            return
        if(evt.typed() == SerialPortSignal.RECEIVE_DATA):
            self.__model__.send(str(evt.value()))
        pass