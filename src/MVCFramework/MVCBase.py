'''
Created on Mar 5, 2012

@author: Aduen
'''
from EventContext import SignalObserver

class ApplicationContext(SignalObserver):
    '''
    Main Context base class
    '''
    
    __context__ = None
    
    def __init__(self):
        if(ApplicationContext.__context__):
            raise ApplicationContext.__context__
        
        SignalObserver.__init__(self, self)
        ApplicationContext.__context__ = self
        pass
        

class Mediator():
    '''
    Mediator base class
    '''
    __context__ = None
    
    def __init__(self):
        self.context = ApplicationContext.__context__
        pass
    

class Controller():
    '''
    Controller base class
    '''
    __context__ = None
    
    def __init__(self):
        self.context = ApplicationContext.__context__
        pass
    
    def onEvent(self, evt):
        pass

class Model():
    '''
    Model base class
    '''
    __context__ = None

    def __init__(self):
        self.context = ApplicationContext.__context__
        pass
