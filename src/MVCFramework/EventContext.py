'''
Created on 14 aug. 2011

@author: Aduen
'''
from threading import Thread
import time

class TimerCall(Thread):
    
    def __init__(self, delay, func, *args):
        Thread.__init__(self)
        self.daemon = False
        
        self.loop = True
        
        self.old_time = time.time()
        self.new_time = self.old_time
        self.delay = delay
        self.func = func
        
        self.args = args
        pass
    
    def stopLoop(self):
        self.loop = False
        #self.join()
    
    def run(self):
        self.old_time = time.time()
        self.new_time = self.old_time
        
        while(self.loop):
            if(self.old_time + self.delay <= self.new_time):
                self.func(*self.args)
                self.loop = False
                
            time.sleep(.001)
            self.new_time = time.time()
        
        print 'exit call timer'

class NotifierThread(Thread):
    
    call_list = []
    invalidated = False
    loop = True
    
    def __init__(self, **kwargs):
        Thread.__init__(self)
    
        self.parameters = kwargs
        self.daemon = False
        
        self.old_time = time.time()
        self.delay = 0.0001
        
    def stopLoop(self):
        self.loop = False
        self.join()
    
    def addNotify(self, evt, func):
        self.call_list.append((func, evt))
        self.invalidated = True
        pass
    
    def run(self):
        while(self.loop or self.invalidated):
            if(self.invalidated):
                try :
                    func, evt = self.call_list.pop(0)
                    func(evt)
                except Exception, e:
                    print 'ERROR: ', e.message, e.args, repr(func), evt
                self.invalidated = len(self.call_list) > 0
            else:
                time.sleep(0.001)
        else:
            print 'leaving Notifier Thread'
            self.call_list = None
        
class SignalObserver():
    
    __signal_thread__ = NotifierThread()
    
    def __init__(self, Source=None):
        self.eventlist = {}
        self.source = None
        
        if(not self.__signal_thread__.isAlive()):
            self.__signal_thread__.start()
        
        if(Source == None):Source = self
        else: self.source = Source
    
    def __hasfunc__(self, call_arr, callback):
        if(getattr(call_arr, '__iter__', False)):
            for i in range(len(call_arr)):
                if(call_arr[i] == callback):
                    return True
        return call_arr == callback
    
    def __hasvalue__(self, value):
        return self.eventlist.has_key(value)
        #return len(self.eventlist.get(value))> 0
    
    def __remove__(self, call_arr, callback):
        if(getattr(call_arr, '__iter__', False)):
            for i in range(len(call_arr)):
                if(call_arr[i] == callback):
                    call_arr.remove(callback)
                    return True
        return False
    
    def __clear__(self):
        self.eventlist = None
        
    def addListener(self, name, handler):
        if (self.eventlist.has_key(name)):
            if(self.__hasfunc__(self.eventlist[name], handler)):
                return 1
            
            call_arr = []
            call_arr.append(handler)
            
            if(getattr(self.eventlist[name], '__iter__', False)):
                call_arr.extend(self.eventlist[name])
            else:
                call_arr.append(self.eventlist[name])
 
            self.eventlist[name] = call_arr
        else :
            self.eventlist[name] = handler;
        return 0
    
    def removeListener(self, name, handler):
        if(not self.__hasvalue__(name)): return False
        
        isIter = self.__remove__(self.eventlist[name], handler)
        #last in line
        if(not isIter):
            if(self.eventlist.has_key(name)):self.eventlist.pop(name)
        return True
            
    def notifyListener(self, typed, value, message='message'):
        if(self.eventlist.has_key(typed)):
            e = Signal(self.source, typed, value, message)
            if(getattr(self.eventlist[typed], '__iter__', False)):
                for i in range(len(self.eventlist.get(typed))):
                    self.__signal_thread__.addNotify(e, self.eventlist.get(typed)[i])
            else:
                self.__signal_thread__.addNotify(e, self.eventlist.get(typed))
    
    #deconstruction            
    def __del__(self):
        print 'exit thread'
        self.__signal_thread__.stopLoop()

#rewrite this shit, what the hell
class Signal():
    """ Main class for signals """
    
    def __init__(self, source, typed, value, message):
        self.__source__ = source
        self.__type__ = typed
        self.__value__ = value
        self.__message__ = message
        
    def source(self):
        return self.__source__
    
    def value(self):
        return self.__value__
    
    def typed(self):
        return self.__type__
    
    def message(self):
        return self.__message__
    
    def __str__(self):
        return 'Signal object -> Source: ' + str(self.source()) + ', Typed: ' + str(self.typed()) + ', Value: ' + str(self.value()) + ', Message: ' + str(self.message())

